package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import javax.print.attribute.standard.NumberOfDocuments;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Categories.ExcludeCategory;
import org.junit.rules.ExpectedException;

public class ClubTest {

	@Test
	public void testEnter() {
		Club a = new Club();
		a.enter(new Person("Marcus", "Schneider", LocalDate.parse("1997-09-22") , Person.Sex.MALE));
		int erg = a.numberOf();
		int erwartet = 1;
		assertEquals(erwartet,erg);
	}

	@Test
	public void testNumberOf() {
		Club a = new Club();
		int erg = a.numberOf();
		int erwartet = 0;
		
		assertEquals(erwartet, erg);
	}
	
	@Test
	public void testAustreten(){
		Club a = new Club();
		
		a.enter(new Person("Test", "Person1", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "Person2", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "Person3", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		
		a.austreten(1);//Test Person2
		//System.out.println(a.getPersons());
		
		int erwartet = 2;
		int erg = a.numberOf();
		
		assertEquals(erwartet,erg);
	}
	
	@Test
	public void testSortieren(){
		Club a = new Club();
		
		a.enter(new Person("Test", "T", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "S", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "O", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		
		a.sortieren();
		System.out.println(a.getPersons());
		
		
		Club expected = new Club();
		
		expected.enter(new Person("Test", "O", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		expected.enter(new Person("Test", "S", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		expected.enter(new Person("Test", "T", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		System.out.println(expected.getPersons());
		
		
		String names1 ="";
		String names2 ="";
		for(int i = 0; i<a.numberOf(); i++){
			names1 +=a.getPersons().get(i).getLastName();
			names2 +=a.getPersons().get(i).getLastName();
		}
		
		assertEquals(names2, names1);
	}
	
	@Test
	public void testDurchschnittsalter1(){
		Club a = new Club();
		a.enter(new Person("Test", "Person1", LocalDate.parse("1995-08-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "Person2", LocalDate.parse("1997-08-27") , Person.Sex.MALE));
		
		int erg = a.durchschnittsalterAlt();
		int expected = 18;
		
		assertEquals(expected, erg);
	}
	@Test
	public void testDurchschnittsalter2(){
		Club a = new Club();
		a.enter(new Person("Test", "Person1", LocalDate.parse("1995-08-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "Person2", LocalDate.parse("1997-08-27") , Person.Sex.MALE));
		
		double erg = a.durchschnittsalterNeu();
		double expected = 18;
		
		assertEquals(expected, erg);
	}
	
	@Test
	public void testChangeFirstName1(){
		Club a = new Club();
		a.enter(new Person("oldName", "Person", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.changeFirstName("newName", 0);
		String expected = "newName";
		
		assertEquals(expected, a.getPersons().get(0).getFirstName());
	}
	
	@Rule public ExpectedException thrown= ExpectedException.none();
	@Test
	public void testChangeFirstName2(){
		thrown.expect(IllegalArgumentException.class);
		Club a = new Club();
		a.enter(new Person("oldName", "Person", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.changeFirstName("", 0);
		String expected = "newName";
	}
	
	@Test
	public void testChangeLastName1(){
		Club a = new Club();
		a.enter(new Person("Test", "oldName", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.changeLastName("newName", 0);
		String expected = "newName";
		
		assertEquals(expected, a.getPersons().get(0).getLastName());
	}
	
	@Rule public ExpectedException thrown1= ExpectedException.none();
	@Test
	public void testChangeLastName2(){
		thrown1.expect(IllegalArgumentException.class);
		
		Club a = new Club();
		a.enter(new Person("Test", "oldName", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.changeLastName("", 0);
		String expected = "newName";
	}
	
	@Test
	public void TestSortName(){
		Club a = new Club();
		a.enter(new Person("Marcus", "A", LocalDate.parse("1997-09-22") , Person.Sex.MALE));
		a.enter(new Person("Marcus", "C", LocalDate.parse("1997-09-22") , Person.Sex.MALE));
		a.enter(new Person("Marcus", "B", LocalDate.parse("1997-09-22") , Person.Sex.MALE));
		a.enter(new Person("Marcus", "D", LocalDate.parse("1997-09-22") , Person.Sex.MALE));
		
		a.sortName();
		List<Person> l = a.getPersons();
		System.out.println(a.getPersons());
		assertTrue(l.get(0).getLastName().equals("A"));
		assertTrue(l.get(1).getLastName().equals("B"));
		assertTrue(l.get(2).getLastName().equals("C"));
		assertTrue(l.get(3).getLastName().equals("D"));
	}
	
	@Test
	public void TestAelteste(){
		Club a = new Club();
		
		a.enter(new Person("Marcus", "Aelteste", LocalDate.parse("1997-08-27") , Person.Sex.MALE));
		a.enter(new Person("Marcus", "B", LocalDate.parse("1998-08-27") , Person.Sex.MALE));
		
		a.sortAelteste();
		
		assertEquals("Aelteste", a.getPersons().get(0).getLastName());
	}

	@Test
	public void TestJuengste1(){
		Club a = new Club();
		
		a.enter(new Person("Marcus", "A", LocalDate.parse("1997-08-27") , Person.Sex.MALE));
		a.enter(new Person("Marcus", "Juengste", LocalDate.parse("1998-08-27") , Person.Sex.MALE));
		
		a.sortJuengste();
		
		assertEquals("Juengste", a.getPersons().get(0).getLastName());
		
	}

}
