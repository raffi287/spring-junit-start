package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;

import javax.print.attribute.standard.NumberOfDocuments;

import org.junit.Ignore;
import org.junit.Test;

public class ClubTest {

	@Test
	public void testEnter() {
		Club a = new Club();
		a.enter(new Person("Marcus", "Schneider", LocalDate.parse("1997-09-22") , Person.Sex.MALE));
		int erg = a.numberOf();
		int erwartet = 1;
		assertEquals(erwartet,erg);
	}

	@Test
	public void testNumberOf() {
		Club a = new Club();
		int erg = a.numberOf();
		int erwartet = 0;
		
		assertEquals(erwartet, erg);
	}
	
	@Test
	public void testAustreten(){
		Club a = new Club();
		
		a.enter(new Person("Test", "Person1", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "Person2", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "Person3", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		
		a.austreten(1);//Test Person2
		//System.out.println(a.getPersons());
		
		int erwartet = 2;
		int erg = a.numberOf();
		
		assertEquals(erwartet,erg);
	}
	
	@Test
	public void testSortieren(){
		Club a = new Club();
		
		a.enter(new Person("Test", "T", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "S", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "O", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		
		a.sortieren();
		System.out.println(a.getPersons());
		
		
		Club expected = new Club();
		
		expected.enter(new Person("Test", "O", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		expected.enter(new Person("Test", "S", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		expected.enter(new Person("Test", "T", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		System.out.println(expected.getPersons());
		
		
		String names1 ="";
		String names2 ="";
		for(int i = 0; i<a.numberOf(); i++){
			names1 +=a.getPersons().get(i).getLastName();
			names2 +=a.getPersons().get(i).getLastName();
		}
		
		assertEquals(names2, names1);
	}
	
	@Test
	public void testDurchschnittsalter(){
		Club a = new Club();
		a.enter(new Person("Test", "Person1", LocalDate.parse("1995-08-27") , Person.Sex.MALE));
		a.enter(new Person("Test", "Person2", LocalDate.parse("1997-08-27") , Person.Sex.MALE));
		
		int erg = a.durchschinittsalter();
		int expected = 18;
		
		assertEquals(expected, erg);
		
		
	}
	
	@Test
	public void testChangeFirstName(){
		Club a = new Club();
		a.enter(new Person("oldName", "Person", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.changeFirstName("newName", 0);
		String expected = "newName";
		
		assertEquals(expected, a.getPersons().get(0).getFirstName());
	}
	
	@Test
	public void testChangeLastName(){
		Club a = new Club();
		a.enter(new Person("Test", "oldName", LocalDate.parse("1997-07-27") , Person.Sex.MALE));
		a.changeLastName("newName", 0);
		String expected = "newName";
		
		assertEquals(expected, a.getPersons().get(0).getLastName());
	}

}
