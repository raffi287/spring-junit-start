package at.spenger.junit.domain;

import java.text.DateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	public void austreten(int i){
		if(l.get(i) == null){
			throw new IllegalArgumentException("Person nicht vorhanden");
		}
		l.remove(i);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public void sortieren(){
		Collections.sort(l, new PersonComparator());
	}
	public class PersonComparator implements Comparator<Person> {
	    @Override
	    public int compare(Person o1, Person o2) {
	        return o1.getLastName().compareTo(o2.getLastName());
	    }
	}
	
	public int durchschinittsalter()
	 {
		if(l.size()== 0){
			return 0;
		}
		GregorianCalendar now = new GregorianCalendar(); 
		DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
	  
		String heute = df.format(now.getTime());
		heute = heute.substring(6,10);
		int h = Integer.parseInt(heute);
	  
		int durchschnitt = 0;
	  
		for(int i = 0;i<l.size();i++)
		{
			String geburt = l.get(i).getBirthdayString();
			geburt = geburt.substring(0,4);
			int g = Integer.parseInt(geburt);
			durchschnitt = durchschnitt+(h-g);
		}
		durchschnitt = durchschnitt/l.size();
	  
		return durchschnitt;
	 }
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}

	
	
	public void changeFirstName(String newName, int index){
		if(l.get(index) == null){
			throw new IllegalArgumentException("Person nicht vorhanden");
		}
		if(newName == null || newName == "" || newName == " "){
			throw new IllegalArgumentException("Kein Name angegeben");
		}
		l.get(index).setFirstName(newName);
	}
	
	public void changeLastName(String newName, int index){
		if(l.get(index) == null){
			throw new IllegalArgumentException("Person nicht vorhanden");
		}
		if(newName == null || newName == "" || newName == " "){
			throw new IllegalArgumentException("Kein Name angegeben");
		}
		l.get(index).setLastName(newName);
	}
}
